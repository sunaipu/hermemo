function map=AutoMakeMaze(size_x, size_y)
tic;
%１が通路, 0:壁
% 左上を始点とし、右下を終点とする
mapsize_x = size_x;
mapsize_y = size_y;

% 全てが0(壁)のmapを作成
map = zeros(mapsize_x,mapsize_y);
way = zeros(4,1);


way(:,1) = 0;

% 現在の座標
position_x = 1;
position_y = 1;

startPointCandidate = zeros(mapsize_x, mapsize_y); % 始点候補を保存, 0:候補ではない, 1:候補である
startPointCandidate(1,1) = 1; % 開始地点を候補に追加

startPointCandidateCount = 1; % 始点候補の数

while 1
    % 始点の候補がなくなった場合迷路は完成
    if startPointCandidateCount == 0, break; end
    
    % 始点を決定
    random = rand;
    cprob = 0;
    for i=1:2:mapsize_x
        for j=1:2:mapsize_y
            if startPointCandidate(i,j) > 0
                cprob = cprob + 1 / startPointCandidateCount;
                if cprob > random
                    % 現在位置を始点に変更
                    position_x = i;
                    position_y = j;
                    break;
                end
            end
        end
        if cprob > random, break; end
    end
    
    while 1
        % 移動可能な候補を保持, 0:移動不可, 1:移動可
        moveCandidate = zeros(4,1); % up, down, right, left
        candidateCount = 0; % 移動可能な数を保持
        
        % 移動可能な方向を検索
        if position_x - 2 >= 1
            if map(position_x - 1, position_y) + map(position_x - 2, position_y) == 0 % up
                moveCandidate(1,1) = 1;
                candidateCount = candidateCount + 1;
            end
        end
        if position_x + 2 <= mapsize_x
            if map(position_x + 1, position_y) + map(position_x + 2, position_y) == 0 % down
                moveCandidate(2,1) = 1;
                candidateCount = candidateCount + 1;
            end
        end
        if position_y - 2 >= 1
            if map(position_x, position_y - 1) + map(position_x, position_y - 2) == 0 % left
                moveCandidate(3,1) = 1;
                candidateCount = candidateCount + 1;
            end
        end
        if position_y + 2 <= mapsize_y
            if map(position_x, position_y + 1) + map(position_x, position_y + 2) == 0 % right
                moveCandidate(4,1) = 1;
                candidateCount = candidateCount + 1;
            end
        end
        
        % 移動可能な方向がない場合
        if candidateCount == 0
            % 今の座標を始点候補から外す
            startPointCandidate(position_x, position_y) = 0;
            % 始点候補の数をデクリメント
            startPointCandidateCount = startPointCandidateCount - 1;
            % 始点を選び直す
            break;
        end
        
        % 移動方向の決定
        random = rand;
        cprob = 0;
        moveWay = 0;
        for i=1:4
            if moveCandidate(i,1) == 1
                cprob = cprob + 1 / candidateCount;
                if cprob > random
                    moveWay = i;
                    break;
                end
            end
        end
        
        % 通路を作成
        switch moveWay
            case 1
                map(position_x - 1, position_y) = 1;
                map(position_x - 2, position_y) = 1;
                startPointCandidate(position_x - 1, position_y) = 1;
                startPointCandidate(position_x - 2, position_y) = 1;
                position_x = position_x - 2;
            case 2
                map(position_x + 1, position_y) = 1;
                map(position_x + 2, position_y) = 1;
                startPointCandidate(position_x + 1, position_y) = 1;
                startPointCandidate(position_x + 2, position_y) = 1;
                position_x = position_x + 2;
                
            case 3
                map(position_x, position_y - 1) = 1;
                map(position_x, position_y - 2) = 1;
                startPointCandidate(position_x, position_y - 1) = 1;
                startPointCandidate(position_x, position_y - 2) = 1;
                position_y = position_y - 2;
                
            case 4
                map(position_x, position_y + 1) = 1;
                map(position_x, position_y + 2) = 1;
                startPointCandidate(position_x, position_y + 1) = 1;
                startPointCandidate(position_x, position_y + 2) = 1;
                position_y = position_y + 2;
        end
        
        startPointCandidateCount = startPointCandidateCount + 2;
    end
end
toc
end